def prime(number):
    i = 2
    length = 0
    while i <= number:
        if number % i == 0:
            length += 1

        i += 1

    if length == 1:
        return True
    else:
        return False


def multiples_of_3_and_5(below):
    i = 1
    j = 0
    while i < below:
        if i % 3 == 0 or i % 5 == 0:
            j = i + j

        i += 1
    return j


def even_fibonacci_numbers(first, second):
    # Hey
    total = 0
    while True:
        first, second = second, first + second
        if second >= 4000000:
            break
        if second % 2 == 0:
            total += second
    return total


def largest_prime_factor(number):
    """Found a better and faster solution : https://stackoverflow.com/questions/15347174/python-finding-prime-factors"""
    i = 2
    while i <= number:
        if prime(i):
            if number % i == 0:
                print(i)

        i += 1


# print(largest_prime_factor(600851475143))
print()
